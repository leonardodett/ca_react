import styles from "./App.module.scss"
import { Header } from "./components/Header"
import { List } from "./components/List"
import { TarefasProvider } from "./context/TarefasContext"
export default function App() {


  return (
    <>
      <div className={styles.container}>
        <TarefasProvider>
          <Header />
          <List />
        </TarefasProvider>
      </div>
    </>
  )
}
