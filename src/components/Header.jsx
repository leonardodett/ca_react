import styles from "./Header.module.scss"
import { useState } from "react"
import { useTarefasContext } from "../context/TarefasContext"
export function Header(props) {
  let [texto, setTexto] = useState("")
  let { novaTarefa } = useTarefasContext()

  return (
    <header className={styles.inputBox}>
      <input type="text" onInput={(evento) => setTexto(evento.target.value)} />
      <button type="button" onClick={() => novaTarefa(texto)}> Adicionar nova tarefa</button>
    </header>
  )
}