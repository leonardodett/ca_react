import ReactModal from 'react-modal'
import styles from "./Modal.module.scss";
export function Modal({ open, setOpen, texto, removerTarefa }) {

  return (
    <ReactModal
      isOpen={open}
      style={{ overlay: { backgroundColor: "#aaaaaa52" }, content: { height: '150px', width: "400px", margin: 'auto' } }}
      onRequestClose={() => setOpen(false)}
      ariaHideApp={false}
    >

      <p> Você deseja Realmente excluir esse item ? ({texto}) </p>
      <div className={styles.controlButtons}>
        <button className={styles.cancel} onClick={() => { setOpen(false) }}>Não</button>
        <button className={styles.accept} onClick={() => { removerTarefa(texto); setOpen(false) }}>Sim</button>
      </div>

    </ReactModal >
  )
}