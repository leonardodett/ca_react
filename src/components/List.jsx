import styles from "./List.module.scss"
import { ListItem } from "./ListItem"
import { Modal } from "./Modal"
import { useState } from "react"
import { useTarefasContext } from "../context/TarefasContext"
export function List(props) {
  let [open, setOpen] = useState(false)
  let [texto, setTexto] = useState("");
  let { tarefas, removerTarefa } = useTarefasContext();
  return (
    <div className={styles.list}>
      <Modal removerTarefa={removerTarefa} open={open} setOpen={setOpen} texto={texto} />
      {
        tarefas.map((tarefa) => (
          <ListItem text={tarefa} key={tarefa} setOpen={setOpen} setTexto={setTexto} />
        )
        )
      }
    </div>
  )
}