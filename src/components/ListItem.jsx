import styles from "./ListItem.module.scss"
import { useState } from "react"
import { FaTrash } from "react-icons/fa"
export function ListItem(props) {
  let [complete, setComplete] = useState(false)

  function handleComplete() {
    setComplete(state => state == false ? true : false);
  }

  return (
    <div className={`${styles.item} ${complete && styles.complete}`}>
      <input type="checkbox" onChange={handleComplete} />
      {props.text}
      <button
        onClick={
          () => {
            props.setOpen(true)
            props.setTexto(props.text)
          }}
      ><FaTrash /></button>
    </div>
  )
}