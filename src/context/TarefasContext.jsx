import { useContext, useState, createContext, useEffect } from "react";
import { api } from "../services/api"
const TarefasContext = createContext({});

export function TarefasProvider({ children }) {
  let [tarefas, setTarefas] = useState([]) // undefined

  async function getData() {
    let response = await api.get("ToDoController/get");
    setTarefas(response.data);
  }

  useEffect(() => {
    getData();
  }, []);

  async function novaTarefa(novaTarefa) {
    // setTarefas((antigasTarefas) => {
    //   if (antigasTarefas.indexOf(novaTarefa) == "-1") {
    //     return [...antigasTarefas, novaTarefa]
    //   } else {
    //     return [...antigasTarefas]
    //   }

    // }); //spread
    let response = await api.post("ToDoController/set", {
      descricao: novaTarefa ?? ""
    });
    getData();
  }
  async function removerTarefa(t) {
    // let nTarefas = tarefas.filter((tar) => tar !== t)
    // setTarefas(nTarefas);
    let response = await api.post("ToDoController/delete", {
      descricao: t ?? ""
    });
    getData();
  }
  return (
    <TarefasContext.Provider
      value={{
        tarefas,
        setTarefas,
        novaTarefa,
        removerTarefa
      }}
    >
      {children}
    </TarefasContext.Provider>
  )

}

export const useTarefasContext = () => useContext(TarefasContext)